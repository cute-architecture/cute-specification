# The CUTE Specification

> The CUTE (CUTting Edge) architecture is intended to be a modern, innovative
ISA originally inspired by the EDGE architecture class.

## License
Specification, documentation, logos and diagrams are licensed under
[CC BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
