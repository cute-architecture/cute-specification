# Edge cases, integration test cases
## Legal programs
- 'Special' instructions at the very beginning and end of block, when the block
  is full
  + `BR` (check `o_next_*`), MUST work properly even if the subblock offset is
    not yet loaded when the `BR` instruction is executed
  + `STR`
  + `WRITE`

- 'Special' instructions at the very beginning and end of block, when the block
  is not full
  + `BR` (check `o_next_*`)
  + `STR`
  + `WRITE`

- Regular instructions at the very beginning and end of block, when the block
  is full

- Regular instructions at the very beginning and end of block, when the block
  is not full

- Instructions at the end of block skipped by a `CMP` instruction, when the bloc
  is full
  + Block end properly detected
  + Footer properly fetched

- Instructions at the end of block skipped by a `CMP` instruction, when the bloc
  is not full
  + Block end properly detected
  + Footer properly fetched

- Signed/negative values

- Last input instructions must properly load the values into local registers for
  the first body instructions that use them

- Instructions not fetched in advance, XNODE has to stall

- Instructions fetched in advance
  + Fetching MUST stops as soon as possible (at the end of block)
  + Execution SHOULD happens as fast as possible (one instruction per clock
    cycle)

- Some instructions have already been fetched thanks to another XNODE requesting
  the same block (is this even possible?)

- In-place looping, instructions are already fetched
  + Execution SHOULD happen as fast as possible (one instruction per clock
  cycle)
  + `JMP` instruction MUST still work

- Block discarded in each different XNODE state
  + Block MUST not commit
  + Subblocks MUST be discarded

- Speculative execution of a block is forbidden
  + It MUST NOT be speculatively scheduled or at least its execution is aborted
    before entering `EXECUTION` state (depending on implementation)

- Maximum execution depth of block is limited
  + Block MUST NOT execute too early

- Block is speculatively executed
  + It SHOULD start as soon as possible
  + It MUST wait before committing itself

- Blocks are scheduled quickly
  + There SHOULD be at most one clock cycle between two different blocks are
    scheduled

- Block properly reads from memory a word written by the previous block

- Blocks ends at the same time (within the same clock cycle)

- Maximum number of subblocks
- Not maximum number of subblocks

- Maximum number of `LDR` instructions
- Not maximum number of `LDR` instructions
- No `LDR` instructions
- Maximum number of `READ` instructions
- Not maximum number of `READ` instructions
- No `READ` instructions

- Maximum number of `STR` instructions
- Not maximum number of `STR` instructions
- Maximum number of `WRITE` instructions
- Not maximum number of `WRITE` instructions

- Register dependency between two consecutive blocks
- Memory dependency between two consecutive blocks

- Multiple `STR` instructions within the same block write to the same address
  + Each instructions MUST be committed in order as usual, overwrite memory
  contents

- Multiple `WRITE` instructions within the same block write to the same global
  register
  + Each instructions MUST be committed in order as usual, overwrite register
    contents

- Multiple `LDR` instructions within the same block write to the same local
  register
  + Each instructions MUST execute as usual, overwrite register contents

- Multiple `READ` instructions within the same block write to the same local
  register
  + Each instructions MUST be committed in order as usual

- Early committing of `LDR` and `WRITE` FIFOs for the current XNODE (if/when
  implemented)

- Only one XNODE in the processor

- Test breadth first scheduling strategy

## Illegal programs
- Too many `LDR`/`READ` instructions
- Too many `STR`/`WRITE` instructions
- Too many body instructions
- No branch taken in a block
