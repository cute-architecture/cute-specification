%!TeX program=XeLaTeX
\documentclass[english, a4paper, 11pt]{article}

\usepackage[margin=0.95in]{geometry}

\usepackage{myfontconfig}
\usepackage{mylanguageconfig}
\usepackage{myenglishconfig}

\usepackage{float}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{rotating}
\usepackage[endianness=big]{bytefield} % FIXME: change all bytefields to use little endian
\usepackage{listings}
\usepackage[hidelinks]{hyperref}
% \usepackage{hyperref}
\usepackage{subcaption}
\usepackage{dtk-logos}
\usepackage{tikz}
\usetikzlibrary{shapes, graphs, matrix, backgrounds, fit, arrows, arrows.meta, positioning, decorations.markings}
\usepackage{pgfgantt}
\usepackage{environ}
\usepackage{makecell}

\usepackage[xindy, nopostdot, nomain, toc, acronyms]{glossaries}
\setacronymstyle{footnote}
\input{glossary}

\newcommand{\blockgray}{gray!10}

\newcommand{\colorbitbox}[3]{%
  \rlap{\bitbox{#2}{\color{#1}\rule{\width}{\height}}}%
  \bitbox{#2}{#3}
}

\newcommand{\reservedbitbox}[1]{%
  \colorbitbox{lightgray}{#1}{Reserved}%
}

\newcommand{\smallreservedbitbox}[1]{%
  \colorbitbox{lightgray}{#1}{R}%
}

\newcommand{\wordsize}{32}

\newcommand{\seqarrow}[5]{%
  \draw[->] ([yshift=-2em]#1)  coordinate (#3)
         -- ([yshift=-.7em]#3-|#2) coordinate (#4)
         node[pos=0.5, above, sloped] {\texttt{#5}};
}

\newcommand{\seqarrowlong}[5]{%
  \draw[->] ([yshift=-2em]#1)  coordinate (#3)
         -- ([yshift=-1.4em]#3-|#2) coordinate (#4)
         node[pos=0.5, above, sloped] {\texttt{#5}};
}

\tikzstyle{interrupt}=[
  postaction={
    decorate,
    decoration={
      markings,
      mark=at position 0.5
       with {
        \fill[white] (-0.05,-0.1) rectangle (0.05,0.1);
        \draw (-0.1,0.1) -- (0,-0.1)
              (0,0.1) -- (0.1,-0.1);
       }
    }
  }
]

% TODO: find a better name
\newcommand{\seqline}[2]{%
  \draw[thick, shorten >=-2em] (#1) -- (#1|-#2);
}

\newcommand{\seqlineshort}[2]{%
  \draw[thick, shorten >=-1em] (#1) -- (#1|-#2);
}

\newcommand{\seqlineintterupt}[2]{%
  \draw[thick, interrupt] (#1) -- (#2);
}

\newganttlinktype{subblock}{
  \ganttsetstartanchor{on bottom=0.05} % FIXME: make this value adjustable
  % \ganttsetendanchor{on left}
  \draw[/pgfgantt/link]
    (\xLeft, \yUpper) --
    (\xLeft, \yLower) --
    (\xRight, \yLower);
}

% TODO: add a version number
\title{\vspace{-7ex}The CUTE Processor Architecture\\Binary Format and Instruction Set
Specification}
\author{Romain Fouquet}
\date{September 2020}

\begin{document}
\maketitle

% \begin{figure}[H]
  % \centering
  % \includegraphics[width=0.1\linewidth]{CUTE-logo.pdf}
% \end{figure}

\tableofcontents

\pagebreak
\quote{The CUTE (CUTting Edge) architecture is intended to be a modern,
innovative ISA originally inspired by the EDGE architecture class.}

The key words \enquote{MUST}, \enquote{MUST NOT}, \enquote{REQUIRED},
\enquote{SHALL}, \enquote{SHALL NOT}, \enquote{SHOULD}, \enquote{SHOULD NOT},
\enquote{RECOMMENDED},  \enquote{MAY}, and \enquote{OPTIONAL} in this document
are to be interpreted as described in
\href{https://tools.ietf.org/html/rfc2119}{RFC 2119}.

\section{Architecture Description}
Several \glspl{xnode} execute compiler-generated instruction blocks, called
\glspl{tblock}, in
parallel and speculatively from a single instruction stream.

\glspl{xnode} exchange data \textit{via} global registers.
They read main memory and global registers at the beginning of \gls{tblock} execution
and write back to main memory and global registers.

Speculative execution plays a big role in the architecture.
\glspl{tblock} are indeed aggressively executed in advance, before it is known whether
they should be executed or not.
Their results are committed as soon as the correct execution path is known.
Data dependencies between \glspl{tblock} are explicitly encoded in \gls{tblock} headers by the
compiler and it is possible to disable speculative execution for each individual
\gls{tblock}, for instance for \glspl{tblock} which have side-effects.
\glspl{tblock} that have been wrongly speculatively
executed do not affect the global architecture state, as each \gls{tblock} only
modifies a local state which is committed when the correct execution path is
known.

Global execution is orchestrated by a hardware scheduler, which distributes
\glspl{tblock} over \glspl{xnode} and manages global registers and main memory access.

Each \gls{xnode} is comprised of an ALU, some local registers and two output
FIFOs, used to temporarily store writes to main memory and global registers
during speculative execution.

% FIXME: add a global diagram with the exec nodes and the scheduler

% FIXME: document security model: all EXN in a core can access all data from
% other EXNs (broadcast memory reads)

\section{\gls{xnode} Pipeline}
The \gls{xnode} is based on a 5-stage pipeline, following this diagram:

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\linewidth]{../../ICL/Project/cute-diagrams/cute-diagram-pipeline.pdf}
  \caption{5-stage pipeline}
\end{figure}

\section{Architecture Figures}
\begin{itemize}
  \item Up to 256~\glspl{xnode}
  \item 16~local registers for each \gls{xnode}
  \item 256~global registers
\end{itemize}

\section{Architecture Details}
The CUTE architecture is little-indian (in this specification, least significant
bits are right-aligned).
A byte is defined as a 8-bit byte and a word is a 4-byte word.

%  FIXME: define byte, as 8-bit

\section{Assembler Constraints}\label{sec:asm-constraints}
\begin{itemize}
  \item All reserved-marked bits MUST be set to \texttt{0} in the machine code.
  \item Speculatively executable (marked with the appropriate \gls{xmode})
    sub-\glspl{tblock} must come before the others in the sub-\gls{tblock} list, so they have a
    chance of being speculatively executed.
\end{itemize}

% \pagebreak
\section{Binary Format}
Binary file is comprised of the sequence of \glspl{tblock} for the
\texttt{.text} section and is followed by the \texttt{.rodata} section.

% \gls{tblock} headers should automatically be completed by the assembler (see the
% \nameref{sec:asm-constraints}).

% FIXME: add a bytefield diagram (with \skippedwords)

\subsection{\gls{tblock} Format}\label{subsec:header-format}
% FIXME: maybe reduce READ and LDR instructions to 16 bits to increase code
% density?
% The header is written along the following syntax and comes right before its
% respective instruction block:

\begin{figure}[H]
  \centering
  % FIXME: fix vertical alignment of braces
  \includegraphics[width=0.9\linewidth]{../../ICL/Project/cute-diagrams/cute-diagram-tblock-format.pdf}
  \caption{\gls{tblock} binary format}
\end{figure}

\texttt{Input count} is the number of \texttt{LDR} + \texttt{READ} instructions.

sub-\glspl{tblock} are the \glspl{tblock} a \gls{tblock} can directly branch to; in
\autoref{fig:call-graphs}, the \glspl{tblock} \texttt{B2} and \texttt{B3} are sub-\glspl{tblock}
of the \gls{tblock} \texttt{B1}, but \texttt{B4}, \texttt{B5} and \texttt{B6} are not.

% FIXME: is this also called dependence height? (maybe rename it in this case)
Maximum Execution Depth MUST prevent this \gls{tblock} from being scheduled as long as this
value is greater than or equal to the current \gls{tblock} execution depth.
This is needed to delay speculative execution of \glspl{tblock} that depend on values
provided by other \glspl{tblock}.
A \gls{tblock} \texttt{B2} depends on another \gls{tblock} \texttt{B1} when \texttt{B2} reads
memory or registers \texttt{B1} have written to. Thus, \texttt{B2} cannot
execute before \texttt{B1} has completed
execution, otherwise it would be executing with invalid input values.

For instance, in the left \gls{cfg} in \autoref{fig:call-graphs}, if the \gls{tblock} \texttt{B5}
depends on \texttt{B1}, its \gls{tblock} header must advertise a Maximum Execution
Depth of one.

Note that this feature is redundant with the \gls{tblock} \gls{xmode} for \glspl{tblock}
that depend on values provided by a \gls{tblock} right above them in the \gls{tblock}
\gls{cfg}; in this case, both features MUST be used: the \gls{tblock} \gls{xmode}
must be set to prevent speculative execution and the Maximum Execution Depth of
the sub-\gls{tblock} must be set to zero.

% \pagebreak
\texttt{LDR} instructions, which access main memory, come first so that
\texttt{READ} instructions can execute while memory is accessed, which takes
many cycles.

% TODO: use proper TikZ graphs
\begin{figure}[H]
  \centering
  \begin{subfigure}[b]{0.17\textwidth}
    \centering
    \includegraphics{../../ICL/Project/cute-diagrams/cute-diagram-cfg0.pdf}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.17\textwidth}
    \centering
    \includegraphics{../../ICL/Project/cute-diagrams/cute-diagram-cfg1.pdf}
  \end{subfigure}
  \caption{Two valid \glspl{cfg}}\label{fig:call-graphs}
\end{figure}

Sub-\glspl{tblock} order withing the \gls{tblock} footer is significant: sub-\glspl{tblock} MUST be scheduled in order, so the
one which should be the more likely to be speculatively executed should be
placed in first position by the compiler.

% \subsubsection{Assembly syntax}
% See the \autoref{sec:asm-ex}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\linewidth]{../../ICL/Project/cute-diagrams/cute-diagram-cfg-depth.pdf}
  \caption{Execution depth if the current \gls{tblock} is
  \texttt{B1}}\label{fig:cfg-depth}
\end{figure}

\subsubsection{\gls{tblock} \gls{xmode}}\label{subsubsec:block-exec-mode}
\gls{tblock} \gls{xmode} MUST be one of the following:

\begin{tabular}{c c l}
  XMODE & Code & Description\\
  \midrule
  \texttt{NOEXEC} & \texttt{00} & do not execute\\
  \texttt{NOSPEC} & \texttt{01} & \makecell[l]{prevent speculative execution
    (may be used for \glspl{tblock} which depend\\on the previous one or for operations
    with side-effects,\\like I/O operations)}\\
  \texttt{LOOP} & \texttt{10} & \makecell[l]{prevent speculative execution,
    disable \gls{tblock} input instructions\\execution
    when re-looping and keep local registers state (useful for\\in-place
    looping)}\\
  \texttt{SPEC} & \texttt{11} & allow speculatively execution\\
  \bottomrule
\end{tabular}

\pagebreak
\section{Scheduler Behavior}
\subsection{General Behavior}
As soon as the next sub-\gls{tblock} becomes known, sub-\glspl{tblock} SHOULD abort
execution, making room for new \glspl{tblock} to execute.

If the \gls{xnode} which has just completed execution was running the
current \gls{tblock}, it commits its execution (see below).

When committing of this \gls{xnode} ends, the scheduler marks it as ready and
schedules a new \gls{tblock} if possible.

When an \gls{xnode} advertises that it is ready (its execution has completed
and it has committed if needed, or it has been reset), the scheduler can
schedule a new yet-to-be-executed \gls{tblock}, following its scheduling strategy.

\subsection{Scheduling Order}
The scheduler MUST schedule \glspl{tblock} following a breadth-first search of
the \gls{cfg}.

That is, it gives priority to sub-\glspl{tblock} of the current \gls{tblock}, then to
sub-\glspl{tblock} of the first sub-\gls{tblock} of the current \gls{tblock} etc.

Sub-\glspl{tblock} of a \gls{tblock} are scheduled in the order of the
\gls{tblock} header.

This scheduler order is referred as execution priority.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\linewidth]{../../ICL/Project/cute-diagrams/cute-diagram-cfg-scheduling.pdf}
  \caption{Scheduling order of an example \gls{cfg}}\label{fig:cfg-scheduling}
\end{figure}

\subsection{Global register file access arbitration}
Access to the global register file MUST be granted according to the execution
priority of the \gls{tblock} relatively to the current \gls{tblock}.

\subsection{Memory access arbitration}
Access to memory MUST be granted according to the execution priority of the
\gls{tblock} relatively to the current \gls{tblock}.

\pagebreak
\section{\gls{xnode} Behavior}
\subsection{\gls{xnode} Loading}
Before running the instructions from the \gls{tblock} body, the execution MUST first
execute all \texttt{LDR} instructions, then all \texttt{READ}
instructions from the header.

% FIXME: specify when to load the sub-\gls{tblock} footer

\subsection{\gls{xnode} Committing}
Committing an \gls{xnode} is done by, in order:
\begin{enumerate}
  \item Applying WRITE FIFO contents;
  \item Applying STR FIFO contents.
\end{enumerate}

% \pagebreak
\section{Instruction List}
Each instruction is a \wordsize-bit~word.

Common fields are designed so that they are aligned as much as possible to
simplify decoding synthesis.

\subsection{Assembly Notations}
\subsubsection{Local registers}
\texttt{Ln}, where \texttt{n} is the register number, refers to the
corresponding local register.

\subsubsection{Global registers}
\texttt{Rn}, where \texttt{n} is the register number, refers to the
corresponding global register.

\subsubsection{Immediate value}
Immediate value MUST be prefixed by the corresponding symbol:

\begin{center}
  \begin{tabular}{l l}
    \toprule
    Base & Prefix Symbol\\
    \midrule
    2  & \texttt{b}\\
    10 & \texttt{\#}\\
    16 & \texttt{\$}\\
    \bottomrule
  \end{tabular}
\end{center}

Many immediate instructions feature the capability of using the immediate value
as the upper half word, to speed up register initialization.

\pagebreak
\subsection{\gls{tblock} Input Instructions}
\subsubsection{\gls{xnode} local registers access}
\paragraph{Assembly syntax}
\texttt{READ LD, RS}

\paragraph{Definition}
Load the value from the \texttt{RS} global register into the \texttt{LD} local
register.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \reservedbitbox{15}
      & \bitbox{8}{\tiny{Source\\Global Register}}
      & \bitbox{4}{\tiny{Destination\\Local Register}}\\

    \bitbox{5}{\texttt{00001}}
      & \reservedbitbox{15}
      & \bitbox{8}{\texttt{RS}}
      & \bitbox{4}{\texttt{LD}}
  \end{bytefield}
  \caption{\texttt{READ} Instruction}
\end{figure}

\subsubsection{Main memory load}
% FIXME: add notation and implementation to load only a halfword or a byte
\paragraph{Assembly syntax}
\texttt{LDR LD, [RSAddr]}

\paragraph{Definition}
Load the word from main memory at the address contained in the \texttt{RSAddr}
global register into the \texttt{LD} local register.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \reservedbitbox{15}
      & \bitbox{8}{\tiny{Source Address\\Global Register}}
      & \bitbox{4}{\tiny{Destination\\Local Register}}\\

    \bitbox{5}{\texttt{00010}}
      & \reservedbitbox{15}
      & \bitbox{8}{\texttt{RSAddr}}
      & \bitbox{4}{\texttt{LD}}
  \end{bytefield}
  \caption{\texttt{LDR} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{LDRB LD, [RSAddr]}

\paragraph{Definition}
Load the byte from main memory at the address contained in the \texttt{RSAddr}
global register into the \texttt{LD} local register.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \reservedbitbox{15}
      & \bitbox{8}{\tiny{Source Address\\Global Register}}
      & \bitbox{4}{\tiny{Destination\\Local Register}}\\

    \bitbox{5}{\texttt{00011}}
      & \reservedbitbox{15}
      & \bitbox{8}{\texttt{RSAddr}}
      & \bitbox{4}{\texttt{LD}}
  \end{bytefield}
  \caption{\texttt{LDRB} Instruction}
\end{figure}

\pagebreak
\subsection{\gls{tblock} Body Instructions}
\subsubsection{Operations}
\paragraph{Assembly syntax}
\texttt{OP LD, LSA, LSB} (\texttt{OP} is one of \texttt{ADD}, \texttt{SUB},
\texttt{XOR}, \texttt{AND}, \texttt{OR}, \texttt{LSL}, \texttt{LSR},
\texttt{MUL}, \texttt{SEL})

%  FIXME: properly document select
%  TODO: properly disable << ligature instead
\paragraph{Definition}
LD ← LSA \texttt{OP} LSB (\texttt{OP} is one of \texttt{+}, \texttt{-},
\texttt{\^}, \texttt{\&}, \texttt{|}, \texttt{<}\texttt{<},
\texttt{>}\texttt{>}, \texttt{*},
\texttt{select})

\paragraph{Operation Opcode}
\begin{tabular}{c c}
  \toprule
  Instruction & Opcode\\
  \midrule
  \texttt{ADD} & \texttt{10001}\\
  \texttt{SUB} & \texttt{10010}\\
  \texttt{XOR} & \texttt{10100}\\
  \texttt{AND} & \texttt{11000}\\
  \texttt{OR}  & \texttt{10011}\\
  \texttt{LSL} & \texttt{10100}\\
  \texttt{LSR} & \texttt{10101}\\
  \texttt{MUL} & \texttt{10110}\\
  \texttt{MUL} & \texttt{10111}\\
  \bottomrule
\end{tabular}

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \reservedbitbox{14}
      & \bitbox{4}{\tiny{Source\\Local Register A}}
      & \bitbox{4}{\tiny{Destination\\Local Register}}
      & \bitbox{4}{\tiny{Source\\Local Register B}}\\

    \bitbox{5}{\texttt{Opcode}}
      & \bitbox{1}{\texttt{0}}
      & \reservedbitbox{14}
      & \bitbox{4}{\texttt{LSA}}
      & \bitbox{4}{\texttt{LD}}
      & \bitbox{4}{\texttt{LSB}}
  \end{bytefield}
  \caption{Operation Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{OP LD, LS, \#Imm} (\texttt{OP} is one of \texttt{ADDI}, \texttt{SUBI},
\texttt{XORI}, \texttt{ANDI}, \texttt{ORI}, \texttt{LSLI}, \texttt{LSRI},
\texttt{MULI}, \texttt{SELI})

\paragraph{Definition}
LD ← LS \texttt{OP} Imm (\texttt{OP} is one of \texttt{+}, \texttt{-},
\texttt{\^}, \texttt{\&}, \texttt{|}, \texttt{<}\texttt{<},
\texttt{>}\texttt{>}, \texttt{*},
\texttt{select})

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \bitbox{4}{\tiny{Destination\\Local Register}}
      & \bitbox{4}{\tiny{Source\\Local Register}}\\

    \bitbox{5}{\texttt{Opcode}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\texttt{0}}
      & \bitbox{16}{\texttt{Imm}}
      & \bitbox{4}{\texttt{LD}}
      & \bitbox{4}{\texttt{LS}}
  \end{bytefield}
  \caption{Immediate Operation Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{OP LD, LS, \#Imm} (\texttt{OP} is one of \texttt{ADDUI}, \texttt{SUBUI},
\texttt{XORUI}, \texttt{ANDUI}, \texttt{ORUI}, \texttt{LSLUI}, \texttt{LSRUI},
\texttt{MULUI}, \texttt{SELUI})

\paragraph{Definition}
LD ← LS \texttt{OP} (Imm << 16) (\texttt{OP} is one of \texttt{+}, \texttt{-},
\texttt{\^}, \texttt{\&}, \texttt{|}, \texttt{<}\texttt{<},
\texttt{>}\texttt{>}, \texttt{*},
\texttt{select})

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \bitbox{4}{\tiny{Destination\\Local Register}}
      & \bitbox{4}{\tiny{Source\\Local Register}}\\

    \bitbox{5}{\texttt{Opcode}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\texttt{1}}
      & \bitbox{16}{\texttt{Imm}}
      & \bitbox{4}{\texttt{LD}}
      & \bitbox{4}{\texttt{LS}}
  \end{bytefield}
  \caption{Upper Immediate Operation Instruction}
\end{figure}

\texttt{Up?} enables to treat the immediate value as if it were in the upper
half of the word, allowing faster register initialization.

\pagebreak
\subsubsection{Compare}
\paragraph{Assembly syntax}
\texttt{CMP LD, LSA, Cond, LSB} (cf. \nameref{par:cond-mnemonics})

\paragraph{Definition}
Store the boolean result of the comparison between \texttt{LSA} and \texttt{LSB} in
\texttt{LD}.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \bitbox{4}{\tiny{Condition}}
      & \reservedbitbox{10}
      & \bitbox{4}{\tiny{Source\\Local Register A}}
      & \bitbox{4}{\tiny{Destination\\Local Register}}
      & \bitbox{4}{\tiny{Source\\Local Register B}}\\

    \bitbox{5}{\texttt{00001}}
      & \bitbox{1}{\texttt{0}}
      & \bitbox{4}{\texttt{Cond}}
      & \reservedbitbox{10}
      & \bitbox{4}{\texttt{LSA}}
      & \bitbox{4}{\texttt{LD}}
      & \bitbox{4}{\texttt{LSB}}
  \end{bytefield}
  \caption{\texttt{CMP} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{CMPI LD, LS, Cond, \#Imm} (cf. \nameref{par:cond-mnemonics})

\paragraph{Definition}
Store the boolean result of the comparison between \texttt{\#Imm} and \texttt{LS} in
\texttt{LD}.

% TODO: maybe change layout if it can help save up come HW
% FIXME: is an upper immediate useful?
\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \bitbox{4}{\tiny{Condition}}
      & \reservedbitbox{6} % FIXME
      & \bitbox{8}{Immediate Value}
      & \bitbox{4}{\tiny{Destination\\Local Register}}
      & \bitbox{4}{\tiny{Source\\Local Register}}\\

    \bitbox{5}{\texttt{00001}}
      & \bitbox{1}{\texttt{1}}
      & \bitbox{4}{\texttt{Cond}}
      & \reservedbitbox{6}
      & \bitbox{8}{\texttt{Imm}}
      & \bitbox{4}{\texttt{LD}}
      & \bitbox{4}{\texttt{LS}}
  \end{bytefield}
  \caption{\texttt{CMPI} Instruction}
\end{figure}

\paragraph{Condition Mnemonics}\label{par:cond-mnemonics}
\begin{tabular}{l l l}
  \toprule
   Mnemonic & Condition & Code\\
  \midrule
  \texttt{EQ}  & Equal                             & \texttt{0000}\\
  \texttt{NE}  & Not Equal                         & \texttt{0001}\\
  \texttt{ULT} & Unsigned Lower Than               & \texttt{0010}\\
  \texttt{ULE} & Unsigned Lower Than Or Equal To   & \texttt{0011}\\
  \texttt{UGT} & Unsigned Greater Than             & \texttt{0100}\\
  \texttt{UGE} & Unsigned Greater Than Or Equal To & \texttt{0101}\\
  \texttt{SLT} & Signed Lower Than                 & \texttt{0110}\\
  \texttt{SLE} & Signed Lower Than Or Equal To     & \texttt{0111}\\
  \texttt{SGT} & Signed Greater Than               & \texttt{1000}\\
  \texttt{SGE} & Signed Greater Than Or Equal To   & \texttt{1001}\\
  \bottomrule
\end{tabular}

\pagebreak
\subsubsection{Branch}
\paragraph{Assembly syntax}
\texttt{BR (BIndexF, XMODE, Priority), LS, (BIndexT, XMODE, Priority)}

\paragraph{Definition}
Set the next \gls{tblock} to be executed as the one at index \texttt{BIndexT} within
the \gls{tblock} footer if the least significant bit of \texttt{LS} is set,
at \texttt{BIndexF} otherwise.

\texttt{XMODE} is one of the \gls{tblock} \glspl{xmode} listed in
\autoref{subsubsec:block-exec-mode}.

The lower the \texttt{Priority}, the lower its index in the footer, and the
higher the chance the \gls{tblock} will be speculatively speculated.

% FIXME: document exec tables in the implementation (not specification)
If the \gls{tblock} is being speculatively executed, this SHOULD discard other \glspl{tblock}
(at an index different from \texttt{BIndexT}/\texttt{BIndexF} within the \gls{tblock}
footer), which will not be branched to.

If the \gls{tblock} is the current \gls{tblock}, it must commit itself and branch to the next
\gls{tblock}, when current \gls{tblock} has finished execution.

\gls{tblock} Index is relative to its position within the \gls{tblock} footer.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \reservedbitbox{15}
      & \bitbox{4}{\tiny{\gls{tblock} Index\\True}}
      & \bitbox{4}{\tiny{\gls{tblock} Index\\False}}
      & \bitbox{4}{\tiny{Source\\Local Register}}\\

    \bitbox{5}{\texttt{00010}}
      & \reservedbitbox{15}
      % TODO: maybe try to move these 4 bits left
      & \bitbox{4}{\texttt{BIndexT}}
      & \bitbox{4}{\texttt{BIndexF}}
      & \bitbox{4}{\texttt{LS}}
  \end{bytefield}
  \caption{\texttt{BR} Instruction}
\end{figure}

\pagebreak
% TODO: merge the immediate and non-immediate instructions, and add the
% immediate value to the value of the local register: WRITE RD, LS, #Imm
\subsubsection{Global register store}
\paragraph{Assembly syntax}
\texttt{WRITE RD, LS}

\paragraph{Definition}
Push the value of the \texttt{LS} local register on the \texttt{WRITE} FIFO, to
be written to the \texttt{RD} global register if and when the \gls{tblock} is
eventually committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \reservedbitbox{14}
      & \bitbox{4}{\tiny{Source\\Local Register}}
      & \bitbox{8}{\tiny{Destination\\Global Register}}\\

    \bitbox{5}{\texttt{00100}}
      & \bitbox{1}{\texttt{0}}
      & \reservedbitbox{14}
      & \bitbox{4}{\texttt{LS}}
      & \bitbox{8}{\texttt{RD}}
  \end{bytefield}
  \caption{\texttt{WRITE} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{WRITEI RD, \#Imm}

\paragraph{Definition}
Push the immediate value on the \texttt{WRITE} FIFO, to be written to the
\texttt{RD} global register if and when the \gls{tblock} is eventually
committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \bitbox{8}{\tiny{Destination\\Global Register}}\\

    \bitbox{5}{\texttt{00100}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{0}
      & \bitbox{16}{\texttt{Imm}}
      & \bitbox{8}{\texttt{RD}}
  \end{bytefield}
  \caption{\texttt{WRITEI} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{WRITEUI RD, \#Imm}

\paragraph{Definition}
Push the immediate value on the \texttt{WRITE} FIFO, to be written to the
\texttt{RD} global register if and when the \gls{tblock} is eventually
committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \bitbox{8}{\tiny{Destination\\Global Register}}\\

    \bitbox{5}{\texttt{00100}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{1}
      & \bitbox{16}{\texttt{Imm}}
      & \bitbox{8}{\texttt{RD}}
  \end{bytefield}
  \caption{\texttt{WRITEUI} Instruction}
\end{figure}

\pagebreak
\subsubsection{Main memory store}
% FIXME: add notation and implementation to store only a halfword or a byte
% FIXME: add an offset: STR [LDAddr + #Offset], LS
% TODO: merge the immediate and non-immediate instructions, and add the
% immediate value to the value of the local register: STR [LDAddr, #Imm], LS or
% STR [LDAddr], LS, #Imm
\paragraph{Assembly syntax} \texttt{STR [LDAddr], LS}

\paragraph{Definition}
Push the value from  the \texttt{LS} local register on the \texttt{STR} FIFO, to
be written in main memory at the address contained in \texttt{LDAddr} if and
when the \gls{tblock} is eventually committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \reservedbitbox{18}
      & \bitbox{4}{\tiny{Source\\Local Register}}
      & \bitbox{4}{\tiny{Destination Address\\Local Register}}\\ % FIXME

    \bitbox{5}{\texttt{00011}}
      & \bitbox{1}{\texttt{0}}
      & \reservedbitbox{18}
      & \bitbox{4}{\texttt{LS}}
      & \bitbox{4}{\texttt{LDAddr}}
  \end{bytefield}
  \caption{\texttt{STR} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{STRI [LDAddr], \#Imm}

\paragraph{Definition}
Push the immediate value to the \texttt{STR} FIFO, to be written in main memory
at the address contained in \texttt{LDAddr} if and when the \gls{tblock} is eventually
committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \reservedbitbox{4}
      & \bitbox{4}{\tiny{Destination Address\\Local Register}}\\ % FIXME

    \bitbox{5}{\texttt{00011}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{0}
      & \bitbox{16}{\texttt{Imm}}
      & \reservedbitbox{4}
      & \bitbox{4}{\texttt{LDAddr}}
  \end{bytefield}
  \caption{\texttt{STRI} Instruction}
\end{figure}

\paragraph{Assembly syntax}
\texttt{STRUI [LDAddr], \#Imm}

\paragraph{Definition}
Push the immediate value to the \texttt{STR} FIFO, to be written in main memory
at the address contained in \texttt{LDAddr} if and when the \gls{tblock} is eventually
committed.

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.1em]{\wordsize}
    \bitheader{0-31}\\

    \bitbox{5}{Opcode}
      & \bitbox{1}{\rotatebox{90}{\tiny Imm?}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{\rotatebox{90}{\tiny Up?}}
      & \bitbox{16}{Immediate Value}
      & \reservedbitbox{4}
      & \bitbox{4}{\tiny{Destination Address\\Local Register}}\\ % FIXME

    \bitbox{5}{\texttt{00011}}
      & \bitbox{1}{\texttt{1}}
      & \smallreservedbitbox{1}
      & \bitbox{1}{1}
      & \bitbox{16}{\texttt{Imm}}
      & \reservedbitbox{4}
      & \bitbox{4}{\texttt{LDAddr}}
  \end{bytefield}
  \caption{\texttt{STRUI} Instruction}
\end{figure}

\subsection{Assembler Pseudo-instructions}
\paragraph{Assembly syntax}
\texttt{NOP} \(\implies\) \texttt{ADDI L0, L0, L0}

\paragraph{Assembly syntax}
\texttt{BRI (BIndexF, XMODE, Priority)} \(\implies\)
\texttt{BR (BIndexF, XMODE, Priority), L0, (0, NOSPEC, 0)}

If \texttt{LS == L0}, the assembler MUST NOT add the false side \gls{tblock} to the
\gls{tblock} footer.
\end{document}
